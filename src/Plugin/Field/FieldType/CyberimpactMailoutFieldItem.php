<?php

namespace Drupal\cyberimpact\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'cyberimpact_mailout' field type.
 *
 * @FieldType(
 *   id = "cyberimpact_mailout",
 *   label = @Translation("Cyberimpact mailout"),
 *   description = @Translation("Allows an entity to send a mailout to a Cyberimpact group."),
 *   default_widget = "cyberimpact_mailout_select",
 *   default_formatter = "cyberimpact_mailout_send_default"
 * )
 */
class CyberimpactMailoutFieldItem extends FieldItemBase {

  protected $mailout = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'cyberimpact_group_ids' => '',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'mailout_checkbox_label' => 'Mailout to Cyberimpact contacts',
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'mailout' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ],
    ];

    return [
      'columns' => $columns
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['mailout'] = DataDefinition::create('boolean')
      ->setLabel(t('Mailout'))
      ->setDescription(t('True when an entity is set to mailout to a group.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $groups = cyberimpact_get_groups();
    
    foreach ($groups as $group) {
      $options[$group->id] = $group->title;
    }

    $cyberimpact_url = Url::fromUri('https://app.cyberimpact.com/', array('attributes' => array('target' => '_blank')));

    $element['cyberimpact_group_ids'] = array(
      '#type' => 'select',
      '#title' => t('Cyberimpact Groups'),
      '#multiple' => TRUE,
      '#description' => t('Available Cyberimpact groups. If there are no options, make sure you have created a group at @Cyberimpact first.', array('@Cyberimpact' => Link::fromTextAndUrl('Cyberimpact', $cyberimpact_url)->toString())),
      '#options' => $options,
      '#default_value' => $this->getSetting('cyberimpact_group_ids'),
      '#required' => TRUE,
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $cyberimpact_group_ids = $this->getFieldDefinition()->getSetting('cyberimpact_group_ids');

    if (empty($cyberimpact_group_ids)) {
      $message = t('Select a Cybermpact group to in the Field settings tab before configuring the field instance.');
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
      return $element;
    }

    $instance_settings = $this->definition->getSettings();

    $element['mailout_checkbox_label'] = array(
      '#title' => 'Mailout Checkbox Label',
      '#type' => 'textfield',
      '#default_value' => !empty($instance_settings['mailout_checkbox_label']) ? $instance_settings['mailout_checkbox_label'] : 'Mailout to Cyberimpact contacts',
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    if(!empty($this->value['mailout'])) {
      $this->mailout = TRUE;
      $this->setValue(['mailout' => 0]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);
    
    $entity = $this->getEntity();

    if($entity->isPublished() && $this->mailout) {
      $this->mailout();
    }
  }

  protected function mailout() {
    $system_config = \Drupal::config('system.site');
    $config = \Drupal::config('cyberimpact.email');

    // Entity
    $entity = clone $this->getEntity();
    $title = $entity->getTitle();
    $type = $entity->type->entity->label();
    
    // Set the current field to be true
    $field_name = $this->getFieldDefinition()->getName();
    $entity->$field_name->value = $this->mailout;

    // Sender
    $from_name = $config->get('from_name');

    if(empty($from_name)) {
      $from_name = $system_config->get('name');
    }

    $from_email = $config->get('from_email');

    if(empty($from_email)) {
      $from_email = $system_config->get('mail');
    }

    $reply_to_email = $config->get('reply_to_email');

    if(empty($reply_to_email)) {
      $reply_to_email = $system_config->get('mail');
    }

    // Email
    $subject = $config->get('subject');

    if(empty($subject)) {
      // Set default subject if empty (string)
      $subject = t('@type: @title')->render();
    }

    $subject = t($subject, array('@type' => $type, '@title' => $title));

    // Get the default theme and change it back to active rendering
    $config = \Drupal::config('system.theme');
    $default_theme = \Drupal::service('theme.initialization')->getActiveThemeByName($config->get('default'));
    $active_theme = \Drupal::theme()->getActiveTheme();

    \Drupal::theme()->setActiveTheme($default_theme);

    $template = [
      '#theme' => 'cyberimpact',
      '#entity' => $entity,
    ];

    $fields = $entity->getFields();
    $timezone = date_default_timezone_get();

    if(!empty($fields)) {
      foreach($fields as $field) {
        $field_type = $field->getFieldDefinition()->getType();
        
        if(in_array($field_type, array('date', 'datetime', 'date_recur'))) {
          $datetime_type = $field->getFieldDefinition()->getSetting('datetime_type');

          if($datetime_type == 'datetime') {
            $field_name = $field->getName();

            $value = new \DateTime($entity->$field_name->value, new \DateTimeZone('UTC'));
            $value->setTimezone(new \DateTimeZone($timezone));
            $entity->$field_name->value = sprintf("%sT%s", $value->format('Y-m-d'), $value->format('H:i:s'));
            
            $end_value = new \DateTime($entity->$field_name->end_value, new \DateTimeZone('UTC'));
            $end_value->setTimezone(new \DateTimeZone($timezone));
            $entity->$field_name->end_value = sprintf("%sT%s", $end_value->format('Y-m-d'), $end_value->format('H:i:s'));
          }
        }
      }
    }

    // Get current schema and host
    $scheme_and_host = \Drupal::request()->getSchemeAndHttpHost();

    // Rewrite relative urls with current scheme and domain in links
    preg_match_all('#<a\s.*?(?:href=[\'"](.*?)[\'"]).*?>#is', $entity->body->value, $matches);

    if(!empty($matches)) {
      $scheme_and_host = \Drupal::request()->getSchemeAndHttpHost();

      foreach($matches[1] as $match) {
        if(preg_match("/^\//", $match)) {
          $entity->body->value = str_replace($match, $scheme_and_host.$match, $entity->body->value);
        }
      }
    }

    // Rewrite relative url with current scheme and domain in images
    preg_match_all('#<img\s.*?(?:src=[\'"](.*?)[\'"]).*?/>#is', $entity->body->value, $matches);

    if(!empty($matches)) {
      $scheme_and_host = \Drupal::request()->getSchemeAndHttpHost();

      foreach($matches[1] as $match) {
        if(preg_match("/^\//", $match)) {
          $entity->body->value = str_replace($match, $scheme_and_host.$match, $entity->body->value);
        }
      }
    }

    // Render template
    $body = \Drupal::service('renderer')->render($template);

    // Restore active theme
    \Drupal::theme()->setActiveTheme($active_theme);

    // Send mailing
    $group_ids = $this->getSetting('cyberimpact_group_ids');
    $group_ids = implode(',', array_filter($group_ids));

    $attributes = array(
      'subject' => $subject,
      'language' => 'en_ca',
      'groups' => $group_ids,
      'sendAt' => date('Y-m-d H:i:s'),
      'sendSpeed' => 'regular',
      'replyTo' => $reply_to_email,
      'fromName' => $from_name,
      'mailFrom' => $from_email,
      'bodyHtml' => $body,
      'bodyText' => $body,
      'googleAnalytics' => TRUE,
      'googleAnalyticsCampaign' => $subject,
      'sendToAFriend' => FALSE
    );

    return cyberimpact_create_mailing($attributes);
  }

  /**
   * Returns the field 'mailout' value.
   *
   * @return bool
   */
  public function getMailout() {
    if (isset($this->values['value'])) {
      return ($this->values['value']['mailout'] == 1);
    }

    return NULL;
  }
}