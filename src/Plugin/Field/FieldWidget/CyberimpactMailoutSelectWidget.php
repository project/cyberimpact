<?php

namespace Drupal\cyberimpact\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'cyberimpact_mailout_select' widget.
 *
 * @FieldWidget(
 *   id = "cyberimpact_mailout_select",
 *   label = @Translation("Mailout form"),
 *   field_types = {
 *     "cyberimpact_mailout"
 *   },
 *   settings = {
 *     "placeholder" = "Select Cyberimpact groups(s)."
 *   }
 * )
 */
class CyberimpactMailoutSelectWidget extends WidgetBase {
  
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /* @var $instance \Drupal\cyberimpact\Plugin\Field\FieldType\CyberimpactMailoutFieldItem */
    $instance = $items[0];

    $description = $this->fieldDefinition->getDescription();
    $subscribe_default = $instance->getMailout();

    $element['mailout'] = array(
      '#title' => $this->fieldDefinition->getSetting('mailout_checkbox_label') ?: $this->t('Mailout to Cyberimpact contacts'),
      '#description' => $description,
      '#type' => 'checkbox',
      '#default_value' => ($subscribe_default) ? TRUE : $this->fieldDefinition->isRequired(),
      '#required' => $this->fieldDefinition->isRequired(),
      '#disabled' => $this->fieldDefinition->isRequired(),
    );

    return array('value' => $element);
  }

}