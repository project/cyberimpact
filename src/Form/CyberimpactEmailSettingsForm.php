<?php

namespace Drupal\cyberimpact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Cyberimpact email settings for this site.
 */
class CyberimpactEmailSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cyberimpact_email_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cyberimpact.email'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings
    $system_config = $this->config('system.site');
    $config = $this->config('cyberimpact.email');

    $form['sender'] = [
      '#type' => 'details',
      '#title' => t('Sender Information'),
      '#open' => TRUE,
    ];
    $form['sender']['from_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Name'),
      '#default_value' => $config->get('from_name'),
      '#attributes' => ['placeholder' => $system_config->get('name')],
    ];
    $form['sender']['from_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('From Email'),
      '#default_value' => $config->get('from_email'),
      '#attributes' => ['placeholder' => $system_config->get('mail')],
    ];
    $form['sender']['reply_to_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply-to Email'),
      '#default_value' => $config->get('reply_to_email'),
      '#attributes' => ['placeholder' => $system_config->get('mail')]
    ];

    $form['template'] = [
      '#type' => 'details',
      '#title' => t('Email Template'),
      '#open' => TRUE,
    ];
    $form['template']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('subject'),
      '#attributes' => ['placeholder' => t('@type: @title')],
      '#description' => t('Use @type or @title field names for replacement')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    //TODO: Validate email?
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cyberimpact.email')
      ->set('from_name', $form_state->getValue('from_name'))
      ->set('from_email', $form_state->getValue('from_email'))
      ->set('reply_to_email', $form_state->getValue('reply_to_email'))
      ->set('subject', $form_state->getValue('subject'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
