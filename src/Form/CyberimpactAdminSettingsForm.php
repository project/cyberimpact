<?php

namespace Drupal\cyberimpact\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Configure Cyberimpact settings for this site.
 */
class CyberimpactAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cyberimpact_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cyberimpact.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings
    $config = $this->config('cyberimpact.settings');

    $api_tokens_url = Url::fromUri('https://app.cyberimpact.com/config/api-tokens', array('attributes' => array('target' => '_blank')));

    // API token
    $form['api_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API Token'),
      '#default_value' => $config->get('api_token'),
      '#description' => t('The API token for your Cyberimpact account. Get or generate a valid API key at your @apilink.', array('@apilink' => Link::fromTextAndUrl(t('API tokens page'), $api_tokens_url)->toString()))
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    //TODO: Validate connetion?
    //$ci = cyberimpact_get_api_object();
    //die();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cyberimpact.settings');
    $config->set('api_token', $form_state->getValue('api_token'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
