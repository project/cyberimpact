<?php

namespace Cyberimpact;

use \Exception;

/**
 * Custom Cyberimpact API exception.
 *
 * @package Cyberimpact
 */
class CyberimpactAPIException extends Exception {

  /**
   * @inheritdoc
   */
  public function __construct($message = "", $code = 0, Exception $previous = NULL) {
    // Construct message from JSON if required.
    if (substr($message, 0, 1) == '{') {
      $message_obj = json_decode($message);
      $message = $message_obj->status_code . ': ' . $message_obj->message;
      if (!empty($message_obj->errors)) {
        $message .= ' ' . serialize($message_obj->errors);
      }
    }

    parent::__construct($message, $code, $previous);
  }

}
