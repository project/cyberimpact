<?php

namespace Cyberimpact;

/**
 * Cyberimpact Mailings library.
 *
 * @package Cyberimpact
 */
class CyberimpactMailings extends Cyberimpact {

  /**
   * Send a Cyberimpact mailing.
   *
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *
   * @see https://cyberimpactapiv4.docs.apiary.io/#reference/ressources/mailings/create-a-new-mailing
   */
  public function create($attributes = []) {
    //TODO: Set default parameters
    $parameters = $attributes;

    return $this->request('POST', '/mailings', $parameters);
  }

}