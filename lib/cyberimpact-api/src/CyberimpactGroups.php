<?php

namespace Cyberimpact;

/**
 * Cyberimpact Groups library.
 *
 * @package Cyberimpact
 */
class CyberimpactGroups extends Cyberimpact {

  /**
   * Gets information about all lists/audiences owned by the authenticated account.
   *
   * @param array $parameters
   *   Associative array of optional request parameters.
   *
   * @return object
   *
   * @see https://cyberimpactapiv4.docs.apiary.io/#reference/ressources/groups
   */
  public function getGroups($parameters = []) {
    return $this->request('GET', '/groups', $parameters);
  }

}