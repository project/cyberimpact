<?php

namespace Cyberimpact;

use Cyberimpact\http\CyberimpactHttpClientInterface;
use Cyberimpact\http\CyberimpactCurlHttpClient;
use Cyberimpact\http\CyberimpactGuzzleHttpClient;

/**
 * Cyberimpact library
 */
class Cyberimpact {
  const VERSION = '4.0';

  /**
   * API version.
   *
   * @var string $version
   */
  public $version = self::VERSION;

  /**
   * The HTTP client.
   *
   * @var CyberimpactHttpClientInterface $client
   */
  protected $client;

  /**
   * The REST API endpoint.
   *
   * @var string $endpoint
   */
  protected $endpoint = 'https://apiv4.cyberimpact.com';

  /**
   * The Cyberimpact API token to authenticate with.
   *
   * @var string $api_token
   */
  private $api_token;

  /**
   * @param string $method Authentication method (self::METHOD_BASIC or self::METHOD_JWT)
   * @param string $user Username or Token
   * @param string $password Password for Basic Auth
   * @throws Exception if $method is not basic or jwt
   */
  public function __construct($api_token, $http_options = [], CyberimpactHttpClientInterface $client = NULL) {
    $this->api_token = $api_token;

    if (!empty($client)) {
      $this->client = $client;
    }
    else {
      $this->client = $this->getDefaultHttpClient($http_options);
    }
  }

  /**
   * Makes a request to the Cyberimpact API.
   *
   * @param string $method
   *   The REST method to use when making the request.
   * @param string $path
   *   The API path to request.
   * @param array $parameters
   *   Associative array of parameters to send in the request body.
   * @param bool $returnAssoc
   *   TRUE to return Cyberimpact API response as an associative array.
   *
   * @return mixed
   *   Object or Array if $returnAssoc is TRUE.
   *
   * @throws CyberimpactAPIException
   */
  public function request($method, $path, $parameters = NULL, $returnAssoc = FALSE) {
    // Set default request options with auth header.
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $this->api_token,
      ],
    ];

    // Add trigger error header if a debug error code has been set.
    if (!empty($this->debug_error_code)) {
      $options['headers']['X-Trigger-Error'] = $this->debug_error_code;
    }

    return $this->client->handleRequest($method, $this->endpoint . $path, $options, $parameters, $returnAssoc);
  }

  /**
   * Instantiates a default HTTP client based on the local environment.
   *
   * @param array $http_options
   *   HTTP client options.
   *
   * @return CyberimpactHttpClientInterface
   *   The HTTP client.
   */
  private function getDefaultHttpClient($http_options) {
    // Process HTTP options.
    // Handle deprecated 'timeout' argument.
    if (is_int($http_options)) {
      $http_options = [
        'timeout' => $http_options,
      ];
    }

    // Default timeout is 10 seconds.
    $http_options += [
      'timeout' => 10,
    ];

    $client = NULL;

    // Use cURL HTTP client if PHP version is below 5.5.0.
    // Use Guzzle client otherwise.
    if (version_compare(phpversion(), '5.5.0', '<')) {
      $client = new CyberimpactCurlHttpClient($http_options);
    }
    else {
      $client = new CyberimpactGuzzleHttpClient($http_options);
    }

    return $client;
  }
}