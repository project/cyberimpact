<?php

/**
 * @file
 * Cyberimpact module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

define('CYBERIMPACT_STATUS_SENT', 'sent');
define('CYBERIMPACT_STATUS_SAVE', 'save');
define('CYBERIMPACT_STATUS_PAUSED', 'paused');
define('CYBERIMPACT_STATUS_SCHEDULED', 'scheduled');
define('CYBERIMPACT_STATUS_SENDING', 'sending');

if (!class_exists('Cyberimpact\Cyberimpact')) {
  include_once __DIR__ . '/lib/cyberimpact-api/src/Cyberimpact.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/CyberimpactAPIException.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/CyberimpactGroups.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/CyberimpactMailings.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/http/CyberimpactHttpClientInterface.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/http/CyberimpactCurlHttpClient.php';
  include_once __DIR__ . '/lib/cyberimpact-api/src/http/CyberimpactGuzzleHttpClient.php';
}

/**
 * Instantiates a Cyberimpact library object.
 *
 * @return \Cyberimpact
 *   Drupal Cyberimpact library object.
 */
function cyberimpact_get_api_object($classname = 'Cyberimpact') {
  $cyberimpact = &drupal_static(__FUNCTION__);
  if (isset($cyberimpact) && $cyberimpact instanceof $classname) {
    return $cyberimpact;
  }

  $config = \Drupal::config('cyberimpact.settings');

  if ($config->get('test_mode')) {
    $classname = '\Cyberimpact\Tests\\' . $classname;
  } else {
    $classname = '\Cyberimpact\\' . $classname;
  }

  if (!class_exists($classname)) {
    $message = t('Failed to load Cyberimpact PHP library. Please refer to the installation requirements.');
    \Drupal::logger('cyberimpact')->error($message);
    \Drupal::messenger()->addMessage($message, 'error', FALSE);
    return NULL;
  }

  $api_token = $config->get('api_token');
  if (!strlen($api_token)) {
    \Drupal::logger('cyberimpact')->error('Cyberimpact Error: API Token cannot be blank.');
    return NULL;
  }

  $cyberimpact = new $classname($api_token);

  return $cyberimpact;
}

/**
 * Returns all Cyberimpact groups for a given key. Groups are stored in the cache.
 *
 * @param array $list_ids
 *   An array of list IDs to filter the results by.
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return array
 *   An array of list data arrays.
 */
function cyberimpact_get_groups() {
  $groups = array();

  try {
    $cyberimpact_groups = cyberimpact_get_api_object('CyberimpactGroups');

    if(!empty($cyberimpact_groups)) {
      $results = $cyberimpact_groups->getGroups();
      
      if($results->totalCount > 0) {
        foreach($results->groups as $group) {
          $groups[$group->id] = $group;
        }
      }
    }
  } catch (\Exception $e) {
    \Drupal::logger('cyberimpact')->error('An error occurred requesting groups information from Cyberimpact. "{message}"', array(
      'message' => $e->getMessage()));
  }

  return $groups;
}

/**
 * Implements hook_form_FORM_ID_alter().
 * 
 * Hide allowed number of values
 */
function cyberimpact_form_field_storage_config_edit_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  $storage = $form_state->getStorage();

  /* @var $field_config \Drupal\field\Entity\FieldConfig */
  $field_config = $storage['field_config'];

  $field_type = $field_config->get('field_type');

  if ($field_type == 'cyberimpact_mailout') {
    // Hide the cardinality setting:
    $form['cardinality_container']['cardinality_number']['#default_value'] = 1;
    $form['cardinality_container']['#access'] = FALSE;

    $form['#validate'][] = 'cyberimpact_form_field_ui_field_edit_form_validate';
  }
}

/**
 * Validation handler for cyberimpact_form_field_ui_field_edit_form.
 *
 * Ensure cardinality is set to 1 on cyberimpact_mailout fields.
 */
function cyberimpact_form_field_ui_field_edit_form_validate(&$form, FormStateInterface &$form_state) {
  $storage = $form_state->getStorage();

  /* @var $field_config \Drupal\field\Entity\FieldConfig */
  $field_config = $storage['field_config'];

  if ($field_config->get('field_type') == 'cyberimpact_mailout') {
    if ($form_state->getValue('cardinality_number') != 1) {
      $form_state->setErrorByName('cardinality_number', t('Cardinality on cyberimpact groups fields must be set to 1.'));
    }
  }
}

/**
 * Create (and send) a Cyberimpact mailing.
 *
 * @param array $attributes
 *   An array of attributes to create the mailing.
 *
 * @return bool
 *   TRUE if campaign is sent successfully.
 */
function cyberimpact_create_mailing($attributes = []) {
  $cyberimpact_mailings = cyberimpact_get_api_object('CyberimpactMailings');

  // Send campaign.
  try {
    if(empty($cyberimpact_mailings)) {
      throw new Exception('Cannot send mailing without Cyberimpact API. Check API token has been entered.');
    }

    $result = $cyberimpact_mailings->create($attributes);

    if ($result->status == CYBERIMPACT_STATUS_SCHEDULED) {
      //TODO: Make timezone dyamic or get through API, set to EST for now
      $timestamp = strtotime($result->scheduledOn.' EST');
      $date = \Drupal::service('date.formatter')->format($timestamp);

      $message = t('Cyberimpact mailing "@subject" will be sent on @date.', array(
        '@subject' => $result->subject,
        '@date' => $date
      ));
      
      // Notify user and log action
      \Drupal::logger('cyberimpact')->notice($message);
      \Drupal::messenger()->addMessage($message, 'warning', FALSE);

      return TRUE;
    }
  }
  catch (\Exception $e) {
    \Drupal::logger('cyberimpact')
      ->error('An error occurred while sending to this mailing: {message}', array(
        'message' => $e->getMessage()
      ));
    \Drupal::messenger()->addMessage($e->getMessage(), 'error', FALSE);
  }
  return FALSE;
}

function cyberimpact_theme($existing, $type, $theme, $path) {
  return [
    'cyberimpact' => [
      'variables' => [
        'entity' => NULL
      ],
      'template' => 'cyberimpact',
    ],
  ];
}

/**
 * Implements hook_help().
 */
function cyberimpact_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the cyberimpact module.
    case 'help.page.cyberimpact':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Adds integration with the Cyberimpact mailing service.') . '</p>';
      return $output;

    default:
  }
}
